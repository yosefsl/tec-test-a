import { TestBed } from '@angular/core/testing';

import { PostCommentsService } from './post-comments.service';

describe('PostCommentsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PostCommentsService = TestBed.get(PostCommentsService);
    expect(service).toBeTruthy();
  });
});
