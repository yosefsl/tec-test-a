
import { User } from '../models/user';
import { Post } from '../models/post';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class PostService{

  private URL="https://jsonplaceholder.typicode.com/posts";
  private URL2="https://jsonplaceholder.typicode.com/User";
  private posts: Post[] = [];
  private postsCollaction: AngularFirestoreCollection<Post>;

  constructor( private http:HttpClient, private db:AngularFirestore) 
  {
    this.postsCollaction= db.collection<Post>('Posts')
  }


  searchPostsData():Observable<Post[]>
  {
    // console.log(this.http.get<Post[]>(`${this.URL}`));
    return this.http.get<Post[]>(`${this.URL}`).pipe(
      map(data =>  this.addUserToPosts(data))
    );
  }

getAllUser(): Observable<User[]>{
    const Users = this.http.get<User[]>(`${this.URL2}`);
    return Users;
}
addNewPost(name:string,title:string,body:string)
{
  const post={name:name,
              title:title,
              body:body
            }
  this.db.collection('Posts').add(post);
}

getPost(): Observable<any[]> {
  return this.db.collection('Posts').valueChanges({idField:'postID'});
} 
getOnePost(postID:string):Observable<any>
    {
      console.log("this is the postID: ",postID);
      return this.db.doc(`Posts/${postID}`).get();
      
    }

  deletePostFromCollection(postID)
  {
    console.log("In - Posts.service deletePostFromCollection() ")
    console.log("This is the postID: "+postID)
    this.db.doc(`Posts/${postID}`).delete();
    console.log(postID+" have deleted")
  }
    
    
    updatePost(postID,title:string,name:string, body:string)
    {
      this.db.doc(`Posts/${postID}`).update(
        {
          name:name,
          title:title,
          body:body
        })
    }

  addUserToPosts(data: Post[]):Post[]
  {
    
    const User = this.getAllUser();
    const postsArray =[];
    User.forEach(user => 
      {
        user.forEach(u =>
          {
            data.forEach(post =>
              {
                if(post.userId == u.uid)
                {
                    postsArray.push(
                      {
                        id: post.id,
                        userId:post.userId,
                        title: post.title,
                        body: post.body,
                      })
                }
            })
        })
    })

    return postsArray;
}

addPostsToCollection()
    {
      // console.log("posts service method")
      const User=this.getAllUser();
      const posts= this.http.get<Post[]>(`${this.URL}`);
      let collactionOfPosts:Observable<Post[]>;
      collactionOfPosts= this.postsCollaction.valueChanges();

      collactionOfPosts.forEach(data=>{
        console.log(data.length)
        if(data.length===0)
        {
          posts.forEach(post =>
            {
            post.forEach(singalPost =>
              {
              User.forEach(user =>
                {
               user.forEach(singalUser =>
                {
                 if(singalUser.uid===singalPost.userId)
                 {
                   singalPost.userId=singalUser.uid;
                   this.db.collection('Posts').add(
                     {
                     title: singalPost.title,
                     name:singalUser.uid,
                     body:singalPost.body
                   })
                 }
               })
              })
            })
          })
        }
        else
        {
          console.log("This posts have added already")
        }
      })




    }


}
