import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { BlogPostsService } from '../../services/blog-posts.service';
import { Posts } from '../../models/posts';
import { Comm } from '../../models/comm';
import { BlogPost } from '../../models/blog-post';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-blog-posts',
  templateUrl: './blog-posts.component.html',
  styleUrls: ['./blog-posts.component.css']
})
export class BlogPostsComponent implements OnInit
{
  post$: Observable<BlogPost>;
  message: string;
  dbPosts$: Observable<BlogPost[]>;
  panelOpenState = false;
  blogPosts$:Observable<BlogPost[]>;
  posts$: Posts[];

  userId:string;
  postid:number;
  text:string;

   constructor(private blogpostsService: BlogPostsService,
                private auth:AuthService,
                public router:Router) { }

  ngOnInit()
  {

    this.blogPosts$ = this.blogpostsService.getPosts();
    this.dbPosts$ = this.blogpostsService.getUserPosts(this.auth.getUser().uid);
    this.auth.user.subscribe(
      user => {
        this.userId = user.uid;
       }
    )
  }

  add(title:string,body:string,id:number){

    this.blogpostsService.addpost(this.userId,title,body)
    this.postid = id;
    console.log("Saved for later")
    this.router.navigate(['/savedposts']);


  }


}
