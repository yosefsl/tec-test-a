import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WelcomeEmptyComponent } from './welcome-empty.component';

describe('WelcomeEmptyComponent', () => {
  let component: WelcomeEmptyComponent;
  let fixture: ComponentFixture<WelcomeEmptyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WelcomeEmptyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WelcomeEmptyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
