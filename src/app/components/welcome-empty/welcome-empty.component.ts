import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-welcome-empty',
  templateUrl: './welcome-empty.component.html',
  styleUrls: ['./welcome-empty.component.css']
})
export class WelcomeEmptyComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
