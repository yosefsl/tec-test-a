import { PostCommentsService } from './services/post-comments.service';
import { BlogPostsService } from './services/blog-posts.service';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import {MatCardModule} from '@angular/material/card';
import { FormsModule } from '@angular/forms';
import { MatInputModule, MatSelectModule } from '@angular/material';
import { MatFormFieldModule } from '@angular/material/form-field';
import { HttpClientModule } from '@angular/common/http';
import { environment } from '../environments/environment';

import {MatExpansionModule} from '@angular/material/expansion';
import { AngularFirestoreModule } from '@angular/fire/firestore';
// import { AngularFireStorageModule } from 'angularfire2/storage';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule, AngularFireAuth } from '@angular/fire/auth';
import { AuthService } from './services/auth.service';


import { LoginComponent } from './components/login/login.component';
import { NavComponent } from './components/nav/nav.component';
import { SignupComponent } from './components/signup/signup.component';
import { MainComponent } from './components/main/main.component';
import { SignupSuccessComponent } from './components/signup-success/signup-success.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { WelcomeEmptyComponent } from './components/welcome-empty/welcome-empty.component';


import { RouterModule, Routes } from '@angular/router';
import { BlogPostsComponent } from './components/blog-posts/blog-posts.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { CommentsComponent } from './components/comments/comments.component';
import { SavedpostsComponent } from './components/savedposts/savedposts.component';




const appRoutes: Routes=[
  { path: 'posts', children: [
    { path: '',  component: BlogPostsComponent},
    { path: ':postId/comments', component: CommentsComponent},
    ]
},
{ path: 'savedposts', component: SavedpostsComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'welcomeempty', component: WelcomeEmptyComponent },
  { path: 'welcome', component: WelcomeComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'signupsuceess', component: SignupSuccessComponent },
  { path: 'login', component: LoginComponent },
  { path: 'main', component: MainComponent },
  {path:"", redirectTo:'/login', pathMatch:'full'},
];









@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    SignupComponent,
    LoginComponent,
    MainComponent,
    SignupSuccessComponent,
    WelcomeComponent,
    WelcomeEmptyComponent,
    BlogPostsComponent,
    DashboardComponent,
    CommentsComponent,
    SavedpostsComponent,

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatExpansionModule,
    MatCardModule,
    MatInputModule,
    MatSelectModule,
    MatFormFieldModule,
    FormsModule,
    HttpClientModule,
    AngularFireModule,
    AngularFirestoreModule,
    // AngularFireStorageModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp(environment.firebase),
    RouterModule
    .forRoot(
      appRoutes,
    //   {enableTracing:true}    // this is for debugging only
    )

  ],
  providers: [AngularFireAuth,AuthService,BlogPostsService,PostCommentsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
